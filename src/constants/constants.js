export const SHORTEN_ENDPOINT = "https://cutto-app.herokuapp.com/shorten";

export const ENTER_URL_AND_ALIAS = "Please enter a URL and an alias";
export const URL_SHORTENED_SUCCESSFULLY = "URL Shortned Successfully";
export const SOMETHING_WENT_WRONG = "Something went wrong!";
export const ALIAS_ALREADY_EXISTS = "Entered alias already exists.";
export const NOT_A_VALID_URL = "Not a valid URL";
export const CANNOT_PROCEED_WITH_INVALID_URL = "Can not proceed with invalid URL";


export const ENTER_OR_PASTE_URL = "Please enter or paste the url here";
