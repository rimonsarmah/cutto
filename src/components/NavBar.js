import "../css/NavBar.css";
const NavBar = () => {
  return (
    <div className="nav-bar">
      <div className="nav-bar-brand">Cutto - The URL Shortener</div>
    </div>
  );
};

export default NavBar;
