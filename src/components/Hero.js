import "../css/Hero.css";
import Form from "./Form";
const Hero = () => {
  return (
    <div className="hero">
      <Form />
    </div>
  );
};

export default Hero;
