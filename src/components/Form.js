import "../css/Form.css";
import { useState } from "react";
import axios from "axios";

import * as CONSTANTS from "../constants/constants";

const Form = () => {
  const [url, setUrl] = useState("");
  const [alias, setAlias] = useState("");
  const [shortenButtonText, setShortenButtonText] = useState("Shorten");

  const [shortenedLink, setShortenedLink] = useState("");

  const [inputFormStyle, setInputFormStyle] = useState("form-input-wrapper");

  const [shortenedUrlWrapperStyle, setShortenedrUrlWrapperStyle] = useState(
    "shortened-url-wrapper hide"
  );
  const [shorteningMessage, setShorteningMessage] = useState(
    CONSTANTS.ENTER_URL_AND_ALIAS
  );
  const [shorteningMessageColor, setShorteningMessageColor] = useState("black");

  const submitHandler = (event) => {
    setShortenButtonText("Shortening..");
    event.preventDefault();
    axios
      .post(CONSTANTS.SHORTEN_ENDPOINT, {
        inputUrl: url,
        inputAlias: alias,
      })
      .then((response) => {
        if (response.data.message === "success") {
          setShorteningMessage(CONSTANTS.URL_SHORTENED_SUCCESSFULLY);
          setShorteningMessageColor("green");
          setInputFormStyle("shortener-form-input hide");
          setShortenedrUrlWrapperStyle("shortened-url-wrapper");
          setShortenedLink("short.cuto.link/" + alias);
        } else if (response.data.message === "alias exists") {
          setShorteningMessage(CONSTANTS.ALIAS_ALREADY_EXISTS);
          setShorteningMessageColor("red");
        } else {
          setShorteningMessage(CONSTANTS.SOMETHING_WENT_WRONG);
          setShorteningMessageColor("red");
        }
      })
      .catch((error) => {
        setShorteningMessage(CONSTANTS.SOMETHING_WENT_WRONG);
        setShorteningMessageColor("red");
      });
  };
  return (
    <div className="form-wrapper">
      <h3 style={{ color: shorteningMessageColor }}>{shorteningMessage}</h3>
      <div className={inputFormStyle}>
        <form onSubmit={submitHandler}>
          <input
            className="shortener-form-input wide"
            type="url"
            placeholder={CONSTANTS.ENTER_OR_PASTE_URL}
            name="url"
            value={url}
            onChange={(event) => {
              setUrl(event.target.value);
            }}
            required
          ></input>
          <input
            className="shortener-form-input"
            type="text"
            placeholder="alias"
            name="alias"
            value={alias}
            onChange={(event) => {
              setAlias(event.target.value);
            }}
            minLength="5"
            maxLength="10"
            required
          ></input>
          <button className="shorten-button" id="url-submit">
            {shortenButtonText}
          </button>
        </form>
      </div>
      <div className={shortenedUrlWrapperStyle}>
        <input
          className="shortener-form-input wide"
          type="text"
          name="shortenedUrlOP"
          value={shortenedLink}
          readOnly
        ></input>
      </div>
    </div>
  );
};

export default Form;
